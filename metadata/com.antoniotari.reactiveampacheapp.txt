Categories:Multimedia,Internet
License:GPL-3.0+
Web Site:https://gitlab.com/antoniotari/reactive-ampache/blob/HEAD/README.md
Source Code:https://gitlab.com/antoniotari/reactive-ampache/
Issue Tracker:https://gitlab.com/antoniotari/reactive-ampache/issues

Name:Reactive-Ampache
Auto Name:Power Ampache
Summary:A material design player for Ampache
Description:
A player for Ampache, simply connect to your Ampache instance and enjoy your
music. Features modern Material Design following the latest guidelines.
.

Repo Type:git
Repo:https://gitlab.com/antoniotari/reactive-ampache.git

Build:1.09.18,28
    commit=v1.09.18
    subdir=app
    submodules=yes
    gradle=yes
    prebuild=sed -i -e 's/${LASTFM_API_KEY}/null/g' build.gradle && \
        sed -i -e 's/${AMPACHE_URL}/null/g' build.gradle && \
        sed -i -e 's/${AMPACHE_URL_LOCAL}/null/g' build.gradle && \
        sed -i -e 's/${AMPACHE_USERNAME}/null/g' build.gradle && \
        sed -i -e 's/${AMPACHE_PASSWORD}/null/g' build.gradle

Build:1.09.22,32
    commit=v1.09.22
    subdir=app
    submodules=yes
    gradle=yes
    prebuild=sed -i -e 's/${LASTFM_API_KEY}/null/g' build.gradle && \
        sed -i -e 's/${AMPACHE_URL}/null/g' build.gradle && \
        sed -i -e 's/${AMPACHE_URL_LOCAL}/null/g' build.gradle && \
        sed -i -e 's/${AMPACHE_USERNAME}/null/g' build.gradle && \
        sed -i -e 's/${AMPACHE_PASSWORD}/null/g' build.gradle

Build:1.09.24,34
    commit=v1.09.24
    subdir=app
    submodules=yes
    gradle=yes
    prebuild=sed -i -e 's/${LASTFM_API_KEY}/null/g' build.gradle && \
        sed -i -e 's/${AMPACHE_URL}/null/g' build.gradle && \
        sed -i -e 's/${AMPACHE_URL_LOCAL}/null/g' build.gradle && \
        sed -i -e 's/${AMPACHE_USERNAME}/null/g' build.gradle && \
        sed -i -e 's/${AMPACHE_PASSWORD}/null/g' build.gradle

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:1.09.24
Current Version Code:34
