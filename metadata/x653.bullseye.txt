Categories:Games
License:GPL-3.0
Web Site:
Source Code:https://gitlab.com/x653/bullseye
Issue Tracker:https://gitlab.com/x653/bullseye/issues

Auto Name:Bullseye
Summary:Scoreboard for tournament dart x01
Description:
Scoreboard for tournament dart x01

* one, two or three Player
* 301, 501, etc.
* dart calculator
* checkouts
* averages
* stylish design
.

Repo Type:git
Repo:https://gitlab.com/x653/bullseye

Build:0.1,1
    commit=v0.1
    subdir=app
    gradle=yes

Build:0.2,2
    commit=3187e8c654517b25a675e0a28f6e918514e94b9b
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.2
Current Version Code:2
