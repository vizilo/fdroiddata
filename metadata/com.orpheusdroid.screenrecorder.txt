AntiFeatures:Tracking
Categories:System
License:AGPL-3.0
Web Site:
Source Code:https://github.com/vijai1996/screenrecorder
Issue Tracker:https://github.com/vijai1996/screenrecorder/issues

Auto Name:ScreenCam
Summary:lightweight and functional screen recorder
Description:
ScreenCam doesn't need any root access to record your screen and works on all
phones with Android Lollipop 5.0 and above. You can also record audio along with
the screen recording and get it beautifully combined with the recorded video.

Choose from different resolutions, frames per second and bitrate for the best
choice of quality and size of the video or make use of the app shortcut in
android 7.1 nougat or in any custom launcher supporting app shortcuts.
.

Repo Type:git
Repo:https://github.com/vijai1996/screenrecorder

Build:1.1,4
    commit=a078649ef3c3a6b53d16b6987a70d99818094a4a
    subdir=app
    gradle=yes

Build:1.5.1,8
    commit=f7f58f272725c74968b78e3d9433b7121d80188b
    subdir=app
    gradle=yes
    prebuild=sed -i -e '/google-services/d' -e '/firebase/d' build.gradle ../build.gradle

Build:1.5.3,9
    commit=3bd6fc11c8483723b6c94cffa56207ea9cccea70
    subdir=app
    gradle=yes

Build:1.5.4,10
    commit=70c88e847a6a02fcdbaabbbce36119edb795b36d
    subdir=app
    gradle=yes

Build:1.6.1,13
    commit=4c2f38643e4a87ea7142c76d9c149ce8ed324ce9
    subdir=app
    gradle=yes

Maintainer Notes:
Tracking via Countly setup in app/src/main/java/com/orpheusdroid/screenrecorder/MainActivity.java
for release builds. Either comment out "setupAnalytics();" there or just move endpoint url -- 
ANALYTICS_URL = "http://analytics.orpheusdroid.com"; -- to localhost, see
app/src/main/java/com/orpheusdroid/screenrecorder/Const.java.
.

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:1.6.1
Current Version Code:13
